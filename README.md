# SHEMAT-Suite_Scripts #

Scripts for Pre- and Postprocessing of `SHEMAT-Suite`.

For some Python-Scripts see https://github.com/jjokella/pyshemkf

Feel free to add your scripts for pre- or postprocessing and visualization of SHEMAT-Suite simulations.

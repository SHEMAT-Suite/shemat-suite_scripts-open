# preprocessing #

Python-functions for preprocessing can be found in
https://github.com/jjokella/pyshemkf/blob/master/site-packages/pskf/tools/run/runmodule.py,
especially the functions `check_hashtag`, `change_hashtag_input`,
`read_hashtag_input` and `run_script` may be interesting.

In `convert_to_hdf5.py`, a class is found that can be used to convert
some of the SHEMAT-Suite input arrays into hdf5-files.

## HDF5 Input ##

# Input File Conversion to HDF5-format

The user input for SHEMAT-Suite is provided in an ASCII-file. For reducing the
reading time of larger input files, SHEMAT-Suite provides the option to read 
HDF5 input instead. Theresore, we provide a Python-script for converting the 
ASCII input file, which is created by the user, into an HDF5 input file.

The script `convert_to_hdf5.py` converts parts of the standard ASCII input file
into and HDF5-file called `<input>_conv.h5`. Those parts which are not converted
to hdf5-arrays are stored in a new ASCII-file called `<input>_conv`. 
Most of the input data, especially large arrays, is then read from the 
HDF5-file, which is considerably faster than reading from the ASCII-file.

**How to use it:**

0.) Prerequisite: Python

1.) Download the python scipt `convert_to_hdf5.py` 
from https://git.rwth-aachen.de/SHEMAT-Suite/shemat-suite_scripts-open

2.) Conversion of standard SHEMAT-Suite input file:
`./convert_to_hdf5.py <inputfile>`

3.) Write the name of the newly generated ASCII-file `<inputfile>_conv` 
into the file `shemade.job `.

4.) Execute SHEMAT-Suite.


**Important note:** 

The input of multiple values for e.g. boundary
conditions either has to be written in one line or the number of entries
has to be defined by using the keyword records. Otherwise the converter
will give an error!

Please test the HDF5 input option with your models and report any 
difficulties like the one noted above (e.g. as an issue), 
so that they can be adapted in the future.  
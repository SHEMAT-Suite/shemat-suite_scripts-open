# MIT License
#
# Copyright (c) 2020 SHEMAT-Suite
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#!/usr/bin/env python
"""
SHEMAT_INPUT file converter
The following parts are converted and included into a HDF5 data file:
    * title
    * grid
    * delx
    * dely
    * delz
    * timestep control
    * tunit
    * tstart
    * titer
    * variable step size
    * time periods
    * units
    * bcs: head bcn,head bcd,head bcw,temp bcn,temp bcd,conc bcn,conc bcd,
           epot bcn,epot bcd,pres bcn,pres bcd,pres bcw,satn bcn,satn bcd
    * uindex
    * head init, temp init, pres init, satn init

How to use it:

1.) Copy the python code `convert_to_hdf5.py` from the code directory
to your model directory.

2.) For using the converter, the Python module has to be loaded.

3.) Conversion of standard SHEMAT-Suite input file:
`./convert_to_hdf5.py <inputfile>`

4.) The file shemade.job has to contain the name of the newly
generated ASCII-file `<inputfile>_conv`

5.) Execute SHEMAT-Suite.
"""

import h5py
import sys
import os
import re
import numpy as np
import StringIO

SHEMAT_MARKER = "#"


class SHEMAT_HDF5:
    """SHEMAT input file conversion class"""

    @staticmethod
    def create_with_string(input):
        return SHEMAT_HDF5(StringIO.StringIO(input))

    @staticmethod
    def create_with_filepath(filepath):
        org_file = open(filepath, "r")
        return SHEMAT_HDF5(org_file)

    def __init__(self, org_file):
        """Initalize the class and convert the SHEMAT input file"""
        self._org_file = org_file
        self.__search_for_marker()

    def convert(self,output_filename):
        """Convert dataset to HDF5"""
        self._new_file = open(output_filename, "w")
        h5_filepath = "{0}.h5".format(output_filename)
        self.__create_file(h5_filepath)
        self.__create_groups()
        self.__read_title()
        self.__read_grid()
        self.__read_time()
        self.__read_units()
        self.__read_uindex()
        self.__read_inits()
        self.__read_bcs()
        self.__copy_remaining_input_file()
        self.__link_file(h5_filepath)
        self.__close_file()
        self._new_file.close()

    def __read_grid(self):
        """Read grid group values"""
        self.__read_grid_dimensions()
        self.__read_delta_sizes()

    def __read_inits(self):
        """Read inital values"""
        if self.__marker_exist("head init"):
            self.__read_full_grid_data(
                "head init", "head", "init/head", "float64")
        self.__read_full_grid_data(
            "temp init", "temp", "init/temp", "float64")
        if self.__marker_exist("pres init"):
            self.__read_full_grid_data(
                "pres init", "pres", "init/pres", "float64")
        if self.__marker_exist("satn init"):
            self.__read_full_grid_data(
                "satn init", "satn", "init/satn", "float64")

    def __read_time(self):
        """Read time group values"""
        self.__read_timestep_control()
        variable_step_size_enabled = self.__read_variable_step_size()
        if not variable_step_size_enabled:
            self.__read_time_periods()
        else:
            self.__remove_from_marker_dict("time periods")

    def __create_file(self, filepath):
        """Create a new empty HDF5 file"""
        self._file = h5py.File(filepath, "w")

    def __create_groups(self):
        """Create HDF5 groups"""
        self.__groups = dict()
        self.__groups["grid"] = self._file.create_group("grid")
        self.__groups["time"] = self._file.create_group("time")
        self.__groups["bc"] = self._file.create_group("bc")
        self.__groups["init"] = self._file.create_group("init")

    def __link_file(self, filepath):
        """Create link to datafile"""
        self._new_file.write("\n{0} h5parse data file\n".format(SHEMAT_MARKER))
        self._new_file.write("{0}\n".format(os.path.basename(filepath)))

    def __close_file(self):
        """Close the HDF5 input file"""
        self._file.close()

    def __del__(self):
        """Destructor"""
        self._org_file.close()

    def __read_title(self):
        """Read title string"""
        title = self.__get_content_at_marker("title")[0]
        if title is not None:
            self._file.attrs["title"] = title
        else:
            self._file.attrs["title"] = "no title"

    def __search_for_marker(self):
        """Scan through the input file and store all marker positions"""
        self._org_file.seek(0, 0)
        self._marker_dict = dict()
        line = self._org_file.readline()
        pattern = re.compile("^{0} (.+)$".format(SHEMAT_MARKER))
        while line:
            matcher = re.match(pattern, line)
            if matcher:
                if matcher.group(1) in self._marker_dict:
                    self._marker_dict[matcher.group(1)].append(self._org_file.tell() -
                                                               len(line))
                else:
                    self._marker_dict[matcher.group(1)] = \
                        [self._org_file.tell() - len(line)]
            line = self._org_file.readline()

    def __get_marker_occurrences(self, marker):
        names = []
        for key in self._marker_dict:
            if key.startswith(marker):
                names += self._marker_dict[key]
        return len(names)

    def __marker_exist(self, marker):
        return self.__get_marker_occurrences(marker) > 0

    def __get_content_at_marker(self, marker, nr=0):
        """Return the content of the first line after the given marker line"""
        pos = None
        names = []
        for key in self._marker_dict:
            if key.startswith(marker):
                names += self._marker_dict[key]
        names.sort()
        if len(names) == 0:
            return None, None
        else:
            pos = names[nr]
        self._org_file.seek(pos, 0)
        line = SHEMAT_HDF5.readline_and_remove_comment(self._org_file)
        while line is not None:
            if re.match("^{0} {1}".format(SHEMAT_MARKER, marker), line):
                args_str = line[line.find(marker):len(line)]
                line = SHEMAT_HDF5.readline_and_remove_comment(self._org_file)
                if line is not None:
                    return line, args_str
                else:
                    return None, None
            else:
                line = SHEMAT_HDF5.readline_and_remove_comment(self._org_file)
        return None, None

    @staticmethod
    def __convert_to_numpy(string, dtype=np.float64):
        """Convert a given string to a numpy array. Fortran repeat syntax will
        be expanded."""
        expanded = list()
        string = string.lower().replace("d", "e")
        for element in re.split(r"\s+", string):
            matcher = re.match(r"^(.+?)\*(.+?)$", element)
            if matcher:
                expanded += [matcher.group(2)] * int(matcher.group(1))
            else:
                expanded.append(element)
        return np.array(expanded, dtype=dtype)

    def __read_bcs(self):
        """Read grid group values"""
        for name in ["head bcn", "head bcd", "head bcw",
                     "temp bcn", "temp bcd",
                     "conc bcn", "conc bcd",
                     "epot bcn", "epot bcd",
                     "pres bcn", "pres bcd", "pres bcw",
                     "satn bcn", "satn bcd"]:
            count = self.__get_marker_occurrences(name)
            grp = self.__groups["bc"]

            tmpinds = []
            tmpvals = []
            args = []
            for i in range(count):
                (tmpind, tmpval, arg) = self.__read_bc(name, i)
                tmpinds.append(tmpind)
                tmpvals.append(tmpval)
                args.append(arg)
            if len(tmpinds) > 0:
                for i, tmpind in enumerate(tmpinds):
                    new_name = (name.replace(" ", "_") + "_{0}").format(i + 1)
                    num_records = tmpind.shape[0]
                    dsetinds = grp.create_dataset("i" + new_name, (9, num_records),
                                                  dtype="int")
                    dsetvals = grp.create_dataset(new_name, (3, num_records),
                                                  dtype="float64")
                    for arg in args[i]:
                        if arg != "records" and args[i][arg] is not None:
                            if arg == "bcmy":
                                dsetvals.attrs[arg] = args[i][arg]
                            else:
                                dsetinds.attrs[arg] = args[i][arg]
                    dsetinds[:] = tmpind.transpose()
                    dsetvals[:] = tmpvals[i].transpose()
            self.__remove_from_marker_dict(name)

    def __read_bc(self, name, nr):
        bc_str, args_str = self.__get_content_at_marker(name, nr)
        if bc_str is None:
            return
        args = dict()

        (cbc_i, cbc_j, cbc_k, cbc_bcu, cbc_bctp,
         cbc_pv, cbc_bt, cbc_si, cbc_dir) = range(9)

        pvs = ["head", "temp", "conc", "epot", "pres", "bhpr", "satn"]
        bts = ["bcd", "bcn", "bcw"]
        i_pv = pvs.index(name.split(" ")[0]) + 1
        i_bt = bts.index(name.split(" ")[1]) + 1

        i_dir = -1
        read_simple = False
        args["simple"] = SHEMAT_HDF5.__read_arg(args_str, "simple")
        if args["simple"] is not None:
            read_simple = True
            for i, dir in enumerate(['none', 'left', 'right', 'front', 'back',
                                     'base', 'top']):
                if args["simple"].startswith(dir):
                    i_dir = i
                    break
            else:
                i_dir = int(args["simple"])
            args["simple"] = True

        args["direction"] = SHEMAT_HDF5.__read_arg(args_str, "direction")
        if args["direction"] is not None:
            for i, dir in enumerate(['none', 'left', 'right', 'front', 'back',
                                     'base', 'top']):
                if args["direction"].startswith(dir):
                    i_dir = i
                    break
            else:
                i_dir = int(args["direction"])
        i_dir = max(0, i_dir)
        args["direction"] = i_dir

        args["bcindex"] = SHEMAT_HDF5.__read_arg(args_str, "bcindex")
        i_bcu = 0
        level_bcindex = 0
        if args["bcindex"] is not None:
            if args["bcindex"][0] == "r":
                level_bcindex = 1
            else:
                level_bcindex = 2
                i_bcu = int(args["bcindex"])
            args["bcindex"] = level_bcindex

        args["bcmy"] = SHEMAT_HDF5.__read_arg(args_str, "bcmy")
        d_bcmy = 1.0e+18
        if args["bcmy"] is not None:
            d_bcmy = float(args["bcmy"])
            args["bcmy"] = d_bcmy

        i_si = 0
        read_species = False
        if (i_pv == pvs.index("conc") + 1):
            read_species = True
        args["species"] = SHEMAT_HDF5.__read_arg(args_str, "species")
        if args["species"] is not None:
            i_si = int(args["species"])
            read_species = False
            args["species"] = i_si

        i_bctp = 0
        read_bctp = True
        args["bctp"] = SHEMAT_HDF5.__read_arg(args_str, "bctp")
        if args["bctp"] is not None:
            i_bctp = int(args["bctp"])
            read_bctp = False
            args["bctp"] = i_bctp

        read_bcval = True
        args["value"] = SHEMAT_HDF5.__read_arg(args_str, "value")
        if args["value"] is not None:
            if args["value"][0] == "i":
                read_bcval = False
            args["value"] = False

        l_errign = False
        args["error"] = SHEMAT_HDF5.__read_arg(args_str, "error")
        if args["error"] is not None:
            if args["error"][0] == "i":
                l_errign = True
            args["error"] = True

        if read_simple:
            i_b = 1
            i_e = self._i0
            j_b = 1
            j_e = self._j0
            k_b = 1
            k_e = self._k0
            if i_dir == 1:
                i_records = self._j0 * self._k0
                i_b = 1
                i_e = 1
            elif i_dir == 2:
                i_records = self._j0 * self._k0
                i_b = self._i0
                i_e = self._i0
            elif i_dir == 3:
                i_records = self._i0 * self._k0
                j_b = 1
                j_e = 1
            elif i_dir == 4:
                i_records = self._i0 * self._k0
                j_b = self._j0
                j_e = self._j0
            elif i_dir == 5:
                i_records = self._i0 * self._j0
                k_b = 1
                k_e = 1
            elif i_dir == 6:
                i_records = self._i0 * self._j0
                k_b = self._k0
                k_e = self._k0
        else:
            args["records"] = SHEMAT_HDF5.__read_arg(args_str, "records")
            if args["records"] is None:
                i_records = int(bc_str)
                bc_str = SHEMAT_HDF5.readline_and_remove_comment(
                    self._org_file)
            else:
                i_records = int(args["records"])

        tmpind = np.zeros([i_records, 9], dtype="int")
        tmpval = np.zeros([i_records, 3], dtype="float64")
        tmpind[:, cbc_bcu] = i_bcu
        tmpind[:, cbc_bctp] = i_bctp
        tmpind[:, cbc_pv] = i_pv
        tmpind[:, cbc_bt] = i_bt
        tmpind[:, cbc_si] = i_si
        tmpind[:, cbc_dir] = i_dir
        tmpval[:, 1] = d_bcmy

        if read_simple:
            if read_species:
                raise RuntimeError('error: "species" index missing!!!')
            if level_bcindex == 0 and read_bcval:
                tmpval[:, 0] = self.__convert_to_numpy(bc_str)
            elif level_bcindex == 2:
                tmpval[:, 0] = 0.0
            elif level_bcindex == 1:
                raise RuntimeError(
                    "error: when 'simple=?', then 'bcindex=read' not supported!!!")
            ll = 0
            for k in range(k_b, k_e + 1):
                for j in range(j_b, j_e + 1):
                    for i in range(i_b, i_e + 1):
                        tmpind[ll, cbc_i] = i
                        tmpind[ll, cbc_j] = j
                        tmpind[ll, cbc_k] = k
                        ll += 1
        else:
            tmpval[:, 0] = 0.0

            for ll in range(i_records):
                bc_str = bc_str.lower().replace("d", "e")
                data = map(float, re.split(r"\s+", bc_str))
                if (level_bcindex == 1 and read_bctp and read_species):
                    tmpind[ll, [0, 1, 2, cbc_bcu, cbc_bctp, cbc_si]] = data[:6]
                elif (level_bcindex == 1 and not read_bctp and read_species):
                    tmpind[ll, [0, 1, 2, cbc_bcu, cbc_si]] = data[:5]
                elif (level_bcindex == 1 and read_bctp and not read_species):
                    tmpind[ll, [0, 1, 2, cbc_bcu, cbc_bctp]] = data[:5]
                elif (level_bcindex == 1 and not read_bctp and not read_species):
                    tmpind[ll, [0, 1, 2, cbc_bcu]] = data[:4]
                elif (level_bcindex == 0 and read_bctp and read_species and read_bcval):
                    tmpind[ll, [0, 1, 2]] = data[:3]
                    tmpval[ll, 0] = data[3]
                    tmpind[ll, [cbc_bctp, cbc_si]] = data[4:6]
                elif (level_bcindex == 0 and not read_bctp and read_species and read_bcval):
                    tmpind[ll, [0, 1, 2]] = data[:3]
                    tmpval[ll, 0] = data[3]
                    tmpind[ll, cbc_si] = data[4]
                elif (level_bcindex == 0 and read_bctp and not read_species and read_bcval):
                    tmpind[ll, [0, 1, 2]] = data[:3]
                    tmpval[ll, 0] = data[3]
                    tmpind[ll, cbc_bctp] = data[4]
                elif (level_bcindex == 0 and not read_bctp and not read_species and read_bcval):
                    tmpind[ll, [0, 1, 2]] = data[:3]
                    tmpval[ll, 0] = data[3]
                elif (level_bcindex == 2 and read_bctp and read_species):
                    tmpind[ll, [0, 1, 2, cbc_bctp, cbc_si]] = data[:5]
                elif (level_bcindex == 2 and not read_bctp and read_species):
                    tmpind[ll, [0, 1, 2, cbc_si]] = data[:4]
                elif (level_bcindex == 2 and read_bctp and not read_species):
                    tmpind[ll, [0, 1, 2, cbc_bctp]] = data[:4]
                elif (level_bcindex == 2 and not read_bctp and not read_species):
                    tmpind[ll, [0, 1, 2]] = data[:3]
                elif (level_bcindex == 0 and read_bctp and read_species and not read_bcval):
                    tmpind[ll, [0, 1, 2, cbc_bctp, cbc_si]] = data[:5]
                elif (level_bcindex == 0 and not read_bctp and read_species and not read_bcval):
                    tmpind[ll, [0, 1, 2, cbc_si]] = data[:4]
                elif (level_bcindex == 0 and read_bctp and not read_species and not read_bcval):
                    tmpind[ll, [0, 1, 2, cbc_bctp]] = data[:4]
                elif (level_bcindex == 0 and not read_bctp and not read_species and not read_bcval):
                    tmpind[ll, [0, 1, 2]] = data[:3]
                bc_str = SHEMAT_HDF5.readline_and_remove_comment(
                    self._org_file)
        return tmpind, tmpval, args

    def __read_uindex(self):
        """Read uindex data"""
        self.__read_full_grid_data("uindex", "uindex", "uindex", "int")

    def __read_full_grid_data(self, marker_name, ext_name, dataset_name, dtype):
        """Read data for i0,j0,k0 grid"""
        data_str, args_str = self.__get_content_at_marker(marker_name)
        if data_str is None:
            raise RuntimeError("{0} not defined!".format(marker_name))
        dset = self._file.create_dataset(
            dataset_name, (self._k0, self._j0, self._i0), dtype=dtype)
        ext_data = self.__read_external_data(args_str, ext_name)
        if (ext_data is not None):
            dset[:] = ext_data[:]
        else:
            array = np.zeros(self._i0 * self._j0 * self._k0, dtype=dtype)
            pos = 0
            while pos < array.size:
                new_array = self.__convert_to_numpy(data_str, dtype=dtype)
                array[pos:pos + new_array.size] = new_array[:
                                                            min(new_array.size, array.size - pos)]
                pos += new_array.size
                data_str = SHEMAT_HDF5.readline_and_remove_comment(
                    self._org_file)

            dset[:] = array.reshape((self._k0, self._j0, self._i0))
        self.__remove_from_marker_dict(marker_name)

    def __read_grid_dimensions(self):
        """Read grid dimensions"""
        grid_str = self.__get_content_at_marker("grid")[0]
        if grid_str is None:
            raise RuntimeError("no grid dimensions i0, j0, k0 defined!")
        self._i0, self._j0, self._k0 = map(int, re.split(r"\s+", grid_str))
        self.__remove_from_marker_dict("grid")

    def __read_delta_sizes(self):
        """Read grid delta sizes"""
        grp = self.__groups["grid"]
        for name, size in (("delx", self._i0), ("dely", self._j0),
                           ("delz", self._k0)):
            del_str, args_str = self.__get_content_at_marker(name)
            dset = grp.create_dataset(name, (size,), dtype="float64")
            ext_data = self.__read_external_data(args_str, name)
            if (ext_data is not None):
                dset[:] = ext_data.reshape(size)
            else:
                if del_str is None:
                    raise RuntimeError("no {0} defined!".format(name))
                dset[:] = SHEMAT_HDF5.__convert_to_numpy(del_str)
            self.__remove_from_marker_dict(name)

    def __read_external_data(self, args_str, name):
        """Read data given by external data file"""
        hdf5_filename = SHEMAT_HDF5.__read_arg(args_str, "HDF5")
        if hdf5_filename is not None:
            ext_file = h5py.File(hdf5_filename, "r")
            data = ext_file[name][:]
            ext_file.close()
            return data
        else:
            return None

    def __read_timestep_control(self):
        """Read timestep control values"""
        active = self.__get_content_at_marker("timestep control")[0]
        if active is not None and int(active) == 1:
            thetaf, thetat, thetac, tstart = \
                map(float, SHEMAT_HDF5.readline_and_remove_comment(
                    self._org_file).split(" "))
            self.__groups["time"].attrs["thetaf"] = thetaf
            self.__groups["time"].attrs["thetat"] = thetat
            self.__groups["time"].attrs["thetac"] = thetac
            self.__groups["time"].attrs["tstart"] = tstart
        self.__remove_from_marker_dict("timestep control")

        for name, var_type in (("tunit", float), ("tstart", float),
                               ("titer", int)):
            str = self.__get_content_at_marker(name)[0]
            if str is not None and not SHEMAT_HDF5.empty_str(str):
                self.__groups["time"].attrs[name] = var_type(str)
            self.__remove_from_marker_dict(name)

    def __read_variable_step_size(self):
        """Read variable step size"""
        active = self.__get_content_at_marker("variable step size")[0]
        if active is not None and int(active) == 1:
            delt_data = \
                SHEMAT_HDF5.readline_and_remove_comment(
                    self._org_file).split(" ")
            self.__groups["time"].attrs["delt_start"] = float(delt_data[0])
            self.__groups["time"].attrs["delt_min"] = float(delt_data[1])
            self.__groups["time"].attrs["delt_max"] = float(delt_data[2])
            self.__groups["time"].attrs["max_simtime"] = float(delt_data[3])
            self.__groups["time"].attrs[
                "delt_double"] = int(float(delt_data[4]))
            result = True
        else:
            result = False
        self.__remove_from_marker_dict("variable step size")
        return result

    def __read_time_periods(self):
        """Read time periods"""
        grp = self.__groups["time"]
        str, args_str = self.__get_content_at_marker("time periods")

        if str is None:
            return
        records_arg = SHEMAT_HDF5.__read_arg(args_str, "records")
        if records_arg is not None:
            nperiod = int(records_arg)
        else:
            nperiod = int(str)
            str = SHEMAT_HDF5.readline_and_remove_comment(self._org_file)
        periods = list()
        for i in range(nperiod):
            new_period = [0.0, 0.0, 0, 0]
            period_elements = re.split("\s+", str)
            if "lin" in period_elements:
                new_period[3] = 1
            elif "log" in period_elements:
                new_period[3] = 2
            else:
                new_period[3] = int(period_elements[3])
            if "inc" in period_elements:
                new_period[3] = abs(new_period[3])
            elif "dec " in period_elements:
                new_period[3] = -abs(new_period[3])
            new_period[0] = float(period_elements[0])
            new_period[1] = float(period_elements[1])
            new_period[2] = int(period_elements[2])
            periods.append(new_period)
            str = SHEMAT_HDF5.readline_and_remove_comment(self._org_file)
        dset = grp.create_dataset("periods", (2, nperiod), dtype="float64")
        dset[:] = np.array([[i[0] for i in periods], [i[1] for i in periods]])
        dset = grp.create_dataset("iperiods", (2, nperiod), dtype="int")
        dset[:] = np.array([[i[2] for i in periods], [i[3] for i in periods]])
        self.__remove_from_marker_dict("time periods")

    @staticmethod
    def __read_arg(args_str, arg_name):
        if args_str is None:
            return None
        first = args_str.find(arg_name)
        if first == -1:
            return None
        else:
            match = re.search(
                r"(?:[,=:;]|\s)+(\S+?)(?:[,=:;]|\s|$)", args_str[first:len(args_str)])
            if match is not None:
                return match.group(1)
            else:
                return ""

    def __read_units(self):
        """Read units"""
        line = self.__get_content_at_marker("units")[0]
        if line is None:
            raise RuntimeError("units not defined")
        unit_data = list()
        while (line is not None) and \
                (len(line) > 0) and \
                (not line.startswith(SHEMAT_MARKER)):
            unit_data.append(SHEMAT_HDF5.__convert_to_numpy(line))
            line = SHEMAT_HDF5.readline_and_remove_comment(self._org_file)

        dset = self._file.create_dataset("units", (len(unit_data),
                                                   len(unit_data[0])),
                                         dtype="float64")
        dset[:] = np.array(unit_data, dtype="float64")
        self.__remove_from_marker_dict("units")

    def __copy_remaining_input_file(self):
        """Copy unconverted parts of the input file to the new input file"""
        positions = []
        map(positions.extend, self._marker_dict.values())
        positions.sort()
        for pos in positions:
            self._org_file.seek(pos, 0)
            line = self._org_file.readline()
            first = True
            while line and (first or not line.startswith(SHEMAT_MARKER)):
                if not line.strip().startswith("!"):
                    self._new_file.write(line)
                line = self._org_file.readline()
                first = False

    def __remove_from_marker_dict(self, marker):
        """Remove marker, given by marker name, from the marker dictionary"""
        del_keys = []
        for key in self._marker_dict:
            if key.startswith(marker):
                del_keys.append(key)
        for del_key in del_keys:
            del self._marker_dict[del_key]

    @staticmethod
    def empty_str(string):
        """Check if string is an empty string"""
        return re.match(r"^\s*$", string)

    @staticmethod
    def readline_and_remove_comment(file):
        """Read a line of the given file and remove comments"""
        line = file.readline()
        if not line:
            return None
        matcher = re.match(r"^([^!]*)(?:!|$)", line.strip())
        if matcher is not None:
            return matcher.group(1).strip()
        else:
            return None

if __name__ == "__main__":
    # Parse command line
    if len(sys.argv) < 2:
        raise RuntimeError("Usage: ./convert_to_hdf5.py <original" +
                           "_shemat_input_file> ")
    if not os.path.isfile(sys.argv[1]):
        raise RuntimeError("\"{0}\" does not exist".format(sys.argv[1]))
    # Convert SHEMAT input file
    shemat_input_file = SHEMAT_HDF5.create_with_filepath(sys.argv[1])
    shemat_input_file.convert("{0}_conv".format(sys.argv[1]))

# postprocessing #

Python-functions for postprocessing can be found in
https://github.com/jjokella/pyshemkf/blob/master/site-packages/pskf/tools/plot/plotfunctions.py

These functions are mostly aimed at reading vtk-files and turning the
result into numpy-arrays.
